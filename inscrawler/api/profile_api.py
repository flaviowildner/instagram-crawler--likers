from typing import List

import requests

from inscrawler.constants.consts import ENTITY_URL


def create_usernames_batch(usernames: List[str]):
    requests.put(f'{ENTITY_URL}/profile/batch',
                 json=[{"username": username} for username in usernames])
