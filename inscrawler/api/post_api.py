from typing import List

import requests

from inscrawler.constants.consts import ENTITY_URL


def get_posts_url_to_crawl() -> List[str]:
    response = requests.get(f'{ENTITY_URL}/post/get_posts_to_crawl', params={'n_posts': 10})
    posts_dto: dict = response.json()
    return [post['url'] for post in posts_dto]
