import logging
from time import sleep
from typing import List

from inscrawler.api.profile_api import create_usernames_batch
from inscrawler.constants.consts import RETRY_TIMEOUT, MAX_RETRY_TIMEOUT


def fetch_likers(browser):
    like_info_btn = browser.find_one("._aam_._aat4 ._aacl._aaco._aacu._aacx._aad6._aade > a")
    if like_info_btn is not None:
        like_info_btn.click()

        likers = {}
        liker_elems_css_selector = "._aacl._aaco._aacu._aacx._aada ._aap6._aap7._aap8 > a"
        likers_elems = list(browser.find(liker_elems_css_selector))
        last_liker = None
        while likers_elems:
            for ele in likers_elems:
                likers[ele.get_attribute("href")] = browser.find_one("span._aacl._aaco._aacw._aacx._aad7._aade",
                                                                     ele).text

            if last_liker == likers_elems[-1]:
                break

            last_liker = likers_elems[-1]
            last_liker.location_once_scrolled_into_view
            sleep(2.0)
            likers_elems = list(browser.find(liker_elems_css_selector))

        # likers_usernames = list([get_or_create_profile(username) for username in likers.values()])
        likers_usernames: List[str] = likers.values()

        retries = 1
        while True:
            try:
                logging.info(f'Saving likers (({len(likers_usernames)}))...')
                create_usernames_batch(likers_usernames)
                break
            except Exception as e:
                logging.exception(e)
                sleep(min(retries * RETRY_TIMEOUT, MAX_RETRY_TIMEOUT))
                retries += 1

        close_btn = browser.find_one("._ab8w._ab94._ab99._ab9f._ab9m._ab9p._ab9y button._abl-")
        if close_btn is not None:
            close_btn.click()
