# -*- coding: utf-8 -*-
from __future__ import unicode_literals

import argparse
import logging
from time import sleep
from typing import List

from inscrawler import InsCrawler
from inscrawler.api.post_api import get_posts_url_to_crawl
from inscrawler.constants.consts import RETRY_TIMEOUT, MAX_RETRY_TIMEOUT
from inscrawler.settings import override_settings
from inscrawler.settings import prepare_override_settings
from logger import start_logger


def usage():
    return """
        python main.py
        python main.py --debug
    """


if __name__ == "__main__":
    parser = argparse.ArgumentParser(
        description="Instagram Crawler - Likers", usage=usage())

    parser.add_argument("--debug", action="store_true")

    prepare_override_settings(parser)

    args = parser.parse_args()

    override_settings(args)

    start_logger()

    ins_crawler = InsCrawler(has_screen=args.debug)
    ins_crawler.login()

    retries = 1
    while True:
        try:
            logging.info("Getting list of posts to crawl...")
            posts_to_crawl_url: List[str] = get_posts_url_to_crawl()
            logging.info(
                f'List of awaiting(to be crawled) posts returned. - Posts : {{{", ".join(posts_to_crawl_url)}}}')
            retries = 1

            post_retries = 1
            for post_to_crawl_url in posts_to_crawl_url:
                try:
                    logging.info(f"Crawling post : \'{post_to_crawl_url}\'...")
                    ins_crawler.get_likers(post_to_crawl_url)
                    logging.info(f"Finished crawling post : \'{post_to_crawl_url}\'!")
                    post_retries = 1
                    sleep(10.0)
                except Exception as e:
                    logging.exception(e)
                    posts_to_crawl_url.append(post_to_crawl_url)
                    sleep(min(post_retries * RETRY_TIMEOUT, MAX_RETRY_TIMEOUT))
                    post_retries += 1

        except Exception as e:
            logging.exception(e)
            sleep(min(retries * RETRY_TIMEOUT, MAX_RETRY_TIMEOUT))
            retries += 1
